#!/usr/bin/python
# -*- coding: utf-8 -*-

# ***************************License:***********************************
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Author: Sajed Rakhshani
# E-mail: SajedRakhshani@msn.com
# gitlab: https://gitlab.com/sajed68/circle-clock-widget
# Start: 13 Mehr 1396
# Third Release 11 Mordad 1397
# version 3.0.0
# Release date: 28 Aban
# #####################################################


from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QMainWindow, QToolTip, QWidget, QMenu, QAction
from PyQt5.QtCore import QDate, QTime, QTime, QTimer
import random
from calendar import *
import math
from settings import *
import utils 
import json
import sys



class CircleClock(QWidget):
    
    
    
    
    
    def __init__(self):
        '''all configs are initiated here:'''
        super(CircleClock, self).__init__()
        
        # time showing option:
        self.clockcolor = [61, 61, 61]
        self.clockfontsize = 45
        self.clockfont_name = "Koodak"
        self.clocktext = " "
        
        # points for second around the clock:
        self.second_color = [255, 0, 0]
        self.seconds_color = [255, 255, 255]
        self.showsecondpoint = False
        
        # date showing:
        self.datetext = " "
        self.datefont_name = "Koodak"
        self.datefontsize = 29
        self.datecolor = [44, 44, 44]
        
        # event tooltip option
        self.eventfontsize = 18
        self.eventfont_name = 'Koodak'
        self.events = {}
        self.holidays = {}
        self.myevents = {}
        self.eventtext= None # reserved
        self.eventtextquery = None # reserved
        
        #clock appearance configs:
        self.clockwidth = 437
        self.clockfacecolor = [66, 255, 255, 130]
        self.clockaroundcirclescolor = [107, 255, 107, 186]
        self.clockradius = None # reserved
        self.clockoffset = None # reserved
        
        # coordinate of positon:
        self.X = 500
        self.Y = 100
        
        # other initializeings:
        self.config = {}
        self.timer = QtCore.QTimer(self)
        self.PI = math.acos(-1)
        self.painter = QtGui.QPainter()
        self.mouse_presspoint = None # reserved
        self.rd = random
        
        #
        self.alpha = [0] * 100
        self.dist = [0] * 100
        self.float_radius = [0] * 100
        self.float_angles = [0] * 100
        self.settings = Settings()
        self.sizechanged = [False]
        
        # Icon on taskbar:
        self.iconontaskborshow = True
        
        self.calendar = Calendar()
        
        ######
        self.clocktext = self.get_dateandtime()
        self.__read_configs__()
        
        self.timer.timeout.connect(self.animate)
        self.timer.start(500)
        
        self.setMinimumWidth(self.clockwidth + self.clockwidth * 0.2)
        self.setMinimumHeight(self.clockwidth + self.clockwidth * 0.2)
        self.setMaximumWidth(self.clockwidth + self.clockwidth * 0.2)
        self.setMaximumHeight(self.clockwidth + self.clockwidth * 0.2)
        
        self.setAttribute(QtCore.Qt.WA_NoSystemBackground, True)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        if self.iconontaskborshow:
            self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        else:
            self.setWindowFlags(QtCore.Qt.FramelessWindowHint|QtCore.Qt.Tool)
            self.settings.setWindowFlags(QtCore.Qt.Tool) 
            self.calendar.setWindowFlags(QtCore.Qt.Tool)
            
        self.init_aroundCircle()
        self.move(self.X, self.Y)
        
        self.settings.get_values_by_reference(self.config, self.events, self.holidays, self.sizechanged)
        self.settings.__update__()  
        
        self.calendar.get_values_by_reference(self.events, self.holidays, self.myevents) 
        self.show()
        self.raise_()
        
          
    def get_dateandtime(self):
        
        datetime = QTime.currentTime()
        date = QDate.currentDate()
        day = date.day()
        month = date.month()
        year = date.year()
        dayofweek = date.dayOfWeek()
        
        shamsi = jdatetime.GregorianToJalali(year, month, day)
        year = str(shamsi.jyear)
        month = str(shamsi.jmonth)
        day = str(shamsi.jday)
 
        self.eventtextquery = utils.english_to_persian(day) + " " + utils.Month(month);
        self.datetext = utils.get_persian_day(dayofweek) + "\n" + self.eventtextquery + " " +\
            utils.english_to_persian(year)

        self.eventtext = self.events.get(self.eventtextquery, None)
        
        if self.eventtext is None:
            self.eventtext = "امروز اتفاقی نیافتاده!";
        reminder = self.myevents.get(self.eventtextquery, None)
        if reminder is not None:
            self.eventtext = self.eventtext + "\n" + "یادآور: " + reminder
            
        return datetime.toString()
        
    
    def paintEvent(self, event):
        self.painter.begin(self)
        self.painter.setRenderHints(QtGui.QPainter.HighQualityAntialiasing | QtGui.QPainter.Antialiasing)
        self.__draw_clock__()
        self.painter.end()
    
    
    def __draw_clock__(self):
        self.clockradius = 150 * self.clockwidth / 350
        self.clockoffset = self.clockwidth / 2 - self.clockradius
        
        self.aroundCircles()

        self.painter.setPen(QtGui.QColor(int(self.clockfacecolor[0]),int(self.clockfacecolor[1]),int(self.clockfacecolor[2]),int(self.clockfacecolor[3])))
        self.painter.setBrush(QtGui.QColor(int(self.clockfacecolor[0]),int(self.clockfacecolor[1]),int(self.clockfacecolor[2]), int(self.clockfacecolor[3])))
        center = QtCore.QPoint(int(self.clockwidth / 2 + self.clockwidth * 0.1), int(self.clockwidth / 2 + self.clockwidth * 0.1))
        self.painter.setCompositionMode(QtGui.QPainter.CompositionMode(2))
        self.painter.drawEllipse(center, self.clockwidth/2-self.clockoffset/2, self.clockwidth/2-self.clockoffset / 2)
        self.painter.setCompositionMode(QtGui.QPainter.CompositionMode(0))
        self.painter.drawEllipse(center, self.clockwidth/2-self.clockoffset/2, self.clockwidth/2-self.clockoffset / 2)
        
        self.__draw_clock_number_points__()
        
        self.painter.setCompositionMode(QtGui.QPainter.CompositionMode(0))
        
        self.painter.setBrush(QtGui.QColor(int(self.seconds_color[0]),int(self.seconds_color[1]),int(self.seconds_color[2])))
        self.painter.setPen(QtGui.QColor(int(self.seconds_color[0]),int(self.seconds_color[1]),int(self.seconds_color[2])))
        self.painter.drawRect(self.clockoffset + self.clockoffset * 0.95 + self.clockwidth * 0.1, self.clockwidth/2 + self.clockwidth * 0.1, 2*self.clockradius - 2*self.clockoffset * 0.95, 1)
        
        self.painter.setBrush(QtGui.QColor(int(self.datecolor[0]), int(self.datecolor[1]), int(self.datecolor[2])))
        self.painter.setCompositionMode(QtGui.QPainter.CompositionMode(0))
        self.painter.setPen(QtGui.QColor(int(self.datecolor[0]), int(self.datecolor[1]), int(self.datecolor[2])))
        self.painter.setFont(QtGui.QFont(self.datefont_name, self.datefontsize))
        
        clockoffset = self.clockoffset
        clockwidth = self.clockwidth
        clockradius = self.clockradius
        
        self.painter.drawText(int(self.clockoffset + self.clockoffset * 0.95 + self.clockwidth * 0.1), int(self.clockwidth/2 + self.clockwidth * 0.1 + 5), int(2*self.clockradius - 2*self.clockoffset * 0.95), int(self.clockwidth / 2 - self.clockoffset), QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop, self.datetext)
        
        self.painter.setBrush(QtGui.QColor(int(self.clockcolor[0]), int(self.clockcolor[1]), int(self.clockcolor[2])))
        self.painter.setPen(QtGui.QColor(int(self.clockcolor[0]), int(self.clockcolor[1]), int(self.clockcolor[2])))
        self.painter.setFont(QtGui.QFont(self.clockfont_name, self.clockfontsize))
        self.painter.drawText(int(clockoffset + clockoffset * 0.95 + clockwidth * 0.1), int(100 + clockoffset+clockwidth*0.1), int(2*clockradius - 2*clockoffset * 0.95), int(clockwidth / 2 - clockoffset), QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop,self.clocktext)
        
    
    def __draw_clock_number_points__(self):
        x1 = int(self.clockwidth / 2 + self.clockwidth * 0.1)
        y1 = int(self.clockwidth / 2 + self.clockwidth * 0.1)
        
        time = self.get_dateandtime()
        ps = int(time[6::]) # second

        if self.showsecondpoint:
            r = int(self.clockwidth / 100)
            R = r + 1
        else:
            R = int(self.clockwidth / 100)
            r = R + 1
        for i in range(60):
            a = (i - 15) * self.PI / 30
            self.painter.setBrush(QtGui.QColor(int(self.seconds_color[0]),int(self.seconds_color[1]),int(self.seconds_color[2])))
            self.painter.setPen(QtGui.QColor(int(self.seconds_color[0]),int(self.seconds_color[1]),int(self.seconds_color[2])))
            c, v = utils.__rotate_the_points__(a, 1, -1*self.clockradius)
            self.painter.drawEllipse(x1 - c - r, y1 - v - r, 2 * r, 2 * r)
            if i == ps:
                self.painter.setBrush(QtGui.QColor(int(self.second_color[0]),int(self.second_color[1]),int(self.second_color[2])))
                self.painter.setPen(QtGui.QColor(int(self.second_color[0]),int(self.second_color[1]),int(self.second_color[2])))
                c, v = utils.__rotate_the_points__(a, 1, -1*self.clockradius);
                if self.showsecondpoint:
                    self.painter.setCompositionMode(QtGui.QPainter.CompositionMode(0))
                else:
                    self.painter.setCompositionMode(QtGui.QPainter.CompositionMode(2))
                self.painter.drawEllipse(x1 - c - R, y1 - v - R, 2 * R, 2 * R)
        
    
    def __update_configs__(self):
        self.clockcolor = self.config['clockcolor']
        self.clockfont_name = self.config['clockfont_name']
        self.clockfontsize = self.config['clockfontsize']
        # read second points on clock:
        self.second_color = self.config['second_color']
        self.seconds_color = self.config['seconds_color']
        self.showsecondpoint = self.config['showsecondpoint']
        # read date showing option:
        self.datefont_name = self.config['datefont_name']
        self.datecolor = self.config['datecolor']
        self.datefontsize = self.config['datefontsize']
        # read clock appearance configs:
        self.clockwidth = self.config['clockwidth']
        self.clockfacecolor = self.config['clockfacecolor']
        self.clockaroundcirclescolor = self.config['clockaroundcirclescolor']
        # read configs for window positioning:
        self.X = self.config['X']
        self.Y = self.config['Y']
        # read configs for event shower tooltip:
        self.eventfont_name = self.config['eventfont_name']
        self.eventfontsize = self.config['eventfontsize']
        # read configs for icon on taskbar:
        self.iconontaskborshow = self.config['icon']
    
    
    def contextMenuEvent(self, event):
        rcmenu = QMenu(self)
        
        quitoption = u"بسته شم؟"
        quit = rcmenu.addAction(quitoption)
        
        calendaropen = u"تقویم را باز کن"
        calendaropenaction = rcmenu.addAction(calendaropen)
        
        settingoption = u"تنظیمات"
        settingmenu = rcmenu.addAction(settingoption)
        
        action = rcmenu.exec_(self.mapToGlobal(event.pos()))
        
        if action == quit:
            self.__write_configs__()
            #self.destroy()
            print("Goodluck")
            self.settings.close()
            self.calendar.close() 
            self.close()
        elif action == calendaropenaction:
            self.calendar.open()
            self.calendar.show()
            
        elif action == settingmenu:
            self.settings.show()
        else:
            print(action)
        
    
    
    def mousePressEvent(self, event):
        self.mouse_presspoint = event.pos()
    
    
    def mouseMoveEvent(self, event):
        x = event.globalX()
        y = event.globalY()
        x_w = self.mouse_presspoint.x()
        y_w = self.mouse_presspoint.y()
        
        self.X = int(x - x_w)
        self.Y = int(y - y_w)
        
        moveto = QtCore.QPoint(self.X, self.Y)
        self.config['X'] = self.X
        self.config['Y'] = self.Y
        self.move(moveto)
    
    
    def aroundCircles(self):
        x1 = self.clockwidth / 2 + self.clockwidth * 0.1
        y1 = self.clockwidth / 2 + self.clockwidth * 0.1
        for n in range(100):
            r = self.float_radius[n]
            self.painter.setPen(QtGui.QColor(int(self.clockaroundcirclescolor[0]),int(self.clockaroundcirclescolor[1]), int(self.clockaroundcirclescolor[2]), int(self.alpha[n] * self.clockaroundcirclescolor[3] / 2)))
            self.painter.setBrush(QtGui.QColor(int(self.clockaroundcirclescolor[0]),int(self.clockaroundcirclescolor[1]), int(self.clockaroundcirclescolor[2]), int(self.alpha[n] * self.clockaroundcirclescolor[3] / 2)))
            self.float_angles[n] -= self.PI / 30
            c, v = utils.__rotate_the_points__(self.float_angles[n], 1, -1*self.clockradius - self.dist[n])
            self.painter.drawEllipse(x1 - c - r, y1 - v - r, 2 * r, 2 * r)
    
    
    def init_aroundCircle(self):
        self.clockradius = 150 * self.clockwidth / 350
        self.clockoffset = self.clockwidth / 2 - self.clockradius
        
        for n in range(100):
            randvar = self.rd.uniform(-100, 100) / 100
            self.float_radius[n] = int(randvar * self.clockoffset / 2.5 + self.clockoffset)
            self.float_angles[n] = (self.rd.uniform(-100, 100)/100 + 1) * 180
            randvar = self.rd.uniform(-100, 100)/100
            self.dist[n] = int(randvar * self.clockoffset/2.5 + self.clockoffset)
            self.alpha[n] = randvar + 1
            
    
    
    def __read_configs__(self):
        
        ## Configs open Start:
        try:
            with open('config.json', 'r') as configfile:
                self.config = json.load(configfile)
            print("Oh! I find config file. so, I read it :)")
            self.__update_configs__()
        except:
            print("Sorry, But, I can't read config file. Is that Exist?")
            print("So, I'm going to create new one by default values!")
            self.__write_configs__()
        ## Configs open END
        ## Events open Start:
        try:
            with open("events.json", "r") as eventfile:
                self.events = json.load(eventfile)
            print("I'm openning Event.json file.")
        except:
            print("I can't find event.json file. please go into settings and use update.")
            self.events = {}
        ## Events open END
        ## Holidays open Start:
        try:
            with open("holidays.json", 'r') as eventfile:
                self.holidays = json.load(eventfile)
            print("I'm openning holidays.json file.")
        except:
            print("There's no holidays.json file. please update.")
            self.holidays = {}
        ## Holidays open END
        ## your events open Start:
        try:
            with open('myevents.json', 'r') as eventfile:
                self.myevents = json.load(eventfile)
            print("Now fetching your reminders!")
        except:
            print("I can't fine myevents.json file. I create an empty one!")
            self.myevents = {}
            with open('myevents.json', 'w') as eventfile:
                json.dump(self.myevents, eventfile)
        ## your events open END
    
    
    def __write_configs__(self):
        self.config['clockcolor'] = self.clockcolor
        self.config['clockfont_name'] = self.clockfont_name
        self.config['clockfontsize'] = self.clockfontsize
        # write second points on clock:
        self.config['second_color'] = self.second_color
        self.config['seconds_color'] = self.seconds_color
        self.config['showsecondpoint'] = self.showsecondpoint
        # write date showing option:
        self.config['datefont_name'] = self.datefont_name
        self.config['datecolor'] = self.datecolor 
        self.config['datefontsize'] = self.datefontsize
        # write clock appearance configs:
        self.config['clockwidth'] = self.clockwidth
        self.config['clockfacecolor'] = self.clockfacecolor
        self.config['clockaroundcirclescolor'] = self.clockaroundcirclescolor
        # write configs for window positioning:
        self.config['X'] = self.X
        self.config['Y'] = self.Y
        # write configs for event shower tooltip:
        self.config['eventfont_name'] = self.eventfont_name
        self.config['eventfontsize'] = self.eventfontsize
        # read configs for icon on taskbar:
        self.config['icon'] = self.iconontaskborshow
        
        
        with open('config.json', 'w') as outfile:
            json.dump(self.config, outfile)
                
        with open('myevents.json', 'w') as outfile:
            json.dump(self.myevents, outfile)
        
    
    
    def animate(self):
        self.clocktext = self.get_dateandtime()

        self.clocktext = utils.english_to_persian(self.clocktext)
        self.__update_configs__()
        QToolTip.setFont(QtGui.QFont(self.eventfont_name, self.eventfontsize))
        self.setToolTip(self.eventtext)
        
        self.settings.get_values_by_reference(self.config, self.events, self.holidays, self.sizechanged)
        self.calendar.get_values_by_reference(self.events, self.holidays, self.myevents)
        if self.sizechanged[0]:
            self.setMinimumWidth(self.clockwidth + self.clockwidth * 0.2)
            self.setMinimumHeight(self.clockwidth + self.clockwidth * 0.2)
            self.setMaximumHeight(self.clockwidth + self.clockwidth * 0.2)
            self.setMaximumWidth(self.clockwidth + self.clockwidth * 0.2)
            self.init_aroundCircle()
            self.sizechanged[0] = not self.sizechanged[0]
        
        self.update()
        
        
    def closeEvent(self, event):
        self.timer.stop()
        self.close()
        event.accept()
        print("Now I'm quitting")
        sys.exit()
    
