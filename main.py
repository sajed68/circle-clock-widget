#!/usr/bin/python
# -*- coding: utf-8 -*-
 
# ***************************License:***********************************
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Author: Sajed Rakhshani
# E-mail: SajedRakhshani@msn.com
# gitlab: https://gitlab.com/sajed68/circle-clock-widget
# Start: 13 Mehr 1396
# First Release 13 Mehr 1396
# Third Release 11 Mordad 1397
# version 3.0.0
# #####################################################
 
from circleclock import *
import sys
from PyQt5.QtWidgets import QApplication
import json


 
 
if __name__ == "__main__":
    a = QApplication(sys.argv)
    w = CircleClock()
    w.show()
    
    a.exec()
    #sys.exit()
