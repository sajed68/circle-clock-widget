#!/usr/bin/python
# -*- coding: utf-8 -*-

# ***************************License:***********************************
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Author: Sajed Rakhshani
# E-mail: SajedRakhshani@msn.com
# gitlab: https://gitlab.com/sajed68/circle-clock-widget
# Start: 13 Mehr 1396
# First Release 13 Mehr 1396
# Third Release 11 Mordad 1397
# version 2.0.1
# Release date: 28 Aban
# #####################################################



from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem 
from PyQt5.QtCore import QDate
import math
from PyQt5 import Qt, QtCore
import jdatetime
import utils
from calendar_ui import Ui_Calendar
from PyQt5.QtGui import QColor



class Calendar(QMainWindow):
    def __init__(self):
        super(Calendar, self).__init__()
        self.events = {}
        self.holidays = {}
        self.myevents = {}
        
        self.currentdayofweek = 0
        self.currentday = 0
        self.currentmonth = 0
        self.currentyear = 0
        self.ui = Ui_Calendar()
        self.ui.setupUi(self)
        self.percurrentdayofweek = 0
        self.percurrentday = 0
        self.percurrentmonth = 0
        self.percurrentyear = 0
        self.isleapyear = False
        self.daynums = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]
        
        self.ui.event_l.setWordWrap(True)
        
        self.ui.caltable.cellClicked.connect(self.caltableclicked)
        self.ui.prevmonth.clicked.connect(self.go_prev_month)
        self.ui.nextmonth.clicked.connect(self.go_next_month)
        self.ui.reminder_b.clicked.connect(self.submit_reminder)
        
        
    def open(self):
        date = QDate.currentDate()
        self.currentday = date.day()
        self.currentdayofweek = date.dayOfWeek()
        self.currentmonth = date.month()
        self.currentyear = date.year()
        self.isleapyear = date.isLeapYear(date.year())
        
        perdate = jdatetime.GregorianToJalali(self.currentyear, self.currentmonth, self.currentday)
        self.percurrentday = perdate.jday
        self.percurrentmonth = perdate.jmonth
        self.percurrentyear = perdate.jyear
        
        self.percurrentdayofweek = (self.currentdayofweek+2)%7
        
        if self.percurrentdayofweek < 0:
            percurrentdayofweek += 7
        
        self.drawtable()
        
        
    def drawtable(self):
        print("I'm going to draw table!")
        if self.isleapyear:
            self.daynums[-1] = 30
        else:
            self.daynums[-1] = 29
        first_day_idx = self.find_start_day_in_month()
        daynum = self.daynums[self.percurrentmonth-1]
        for x in range(1, 7):
            for y in range(0, 7):
                item = QTableWidgetItem()
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                self.ui.caltable.setItem(x, y, item)
        j = 0
        for i in range(0, daynum):
            j = i + first_day_idx
            x = int(j / 7) + 1
            y = j % 7
            if (y < 0):
                y += 7
            item = QTableWidgetItem()
            item.setText(utils.english_to_persian(str(i+1)))
            eventquery = utils.make_eventquery(i+1, self.percurrentmonth)
            if y == 6 or self.holidays.get(eventquery, None) is not None:
                item.setBackground(QColor(255, 85, 85))
                item.setForeground(QColor(255, 255, 255))
            else:
                item.setBackground(QColor(85, 255, 170))
                item.setForeground(QColor(0, 0, 0))
                
            if self.myevents.get(eventquery, None) is not None:
                item.setBackground(QColor(85, 255, 85))
                item.setForeground(QColor(0, 0, 0))
            item.setFlags(QtCore.Qt.ItemIsEnabled)
            self.ui.caltable.setItem(x, y, item)
        self.ui.monthname.setText(utils.Month(self.percurrentmonth))
        
    
    
    def find_start_day_in_month(self):
        fullweek = self.percurrentday + (6 - self.percurrentdayofweek)
        first_day_idx = 7 - fullweek % 7 - 1
        if first_day_idx < 0:
            first_day_idx += 7
        return first_day_idx
    
    
    def caltableclicked(self): ##TODO
        day = self.ui.caltable.currentItem().text()
        x = self.ui.caltable.currentRow()
        month = self.ui.monthname.text()
        eventquery = day + " " + month
        eventtext = ""
        ev1 = self.events.get(eventquery, None)
        ev2 = self.myevents.get(eventquery, None)
        if ev1 is not None:
            eventtext += ev1
        if ev2 is not None:
            eventtext += '\n' + u'یادآور:' + ev2
        self.ui.event_l.setText(eventtext)
        if len(day) !=0 and x >= 1:
            self.ui.reminder_l.setText(eventquery)
            self.ui.reminder_b.setVisible(True)
            self.ui.reminder_l.setVisible(True)
            self.ui.reminder_e.setVisible(True)
            self.ui.reminder_b.setEnabled(True)
            self.ui.reminder_e.setText("")
        else:
            self.ui.reminder_l.setText(" ")
            self.ui.reminder_b.setVisible(False)
            self.ui.reminder_l.setVisible(False)
            self.ui.reminder_e.setVisible(False)
            self.ui.reminder_b.setEnabled(False)
            self.ui.reminder_e.setText("")
        
    
    
    def go_prev_month(self):
        if self.percurrentmonth > 1:
            first_day_idx = self.find_start_day_in_month()
            if first_day_idx !=0:
                last_day_pred_month = first_day_idx - 1
            else:
                last_day_pred_month = 6
            self.percurrentmonth -= 1
            self.percurrentday = self.daynums[self.percurrentmonth - 1]
            self.percurrentdayofweek = last_day_pred_month + 1
            
            self.drawtable()
    
    
    def go_next_month(self):
        if self.percurrentmonth < 12:
            first_day_idx = self.find_start_day_in_month()
            last_day_idx = self.daynums[self.percurrentmonth-1] % 7
            if last_day_idx < 0 :
                last_day_idx += 7
            last_day_idx = last_day_idx + first_day_idx - 1
            if last_day_idx > 6:
                last_day_idx -= 7
            if last_day_idx < 0:
                last_day_idx += 7
            first_day_next_month_idx = last_day_idx + 1
            if first_day_next_month_idx > 6:
                first_day_next_month_idx -= 7
            self.percurrentday = 1
            self.percurrentdayofweek = first_day_next_month_idx + 1
            self.percurrentmonth += 1
            
            self.drawtable()
    
    
    def submit_reminder(self):
        text = self.ui.reminder_e.text()
        if len(text) !=0 :
            query = self.ui.reminder_l.text()
            self.myevents[query] = self.ui.reminder_e.text()
            self.ui.reminder_e.setText("")
        else:
            try:
                self.myevents.pop(self.ui.reminder_l.text())
                print("your reminder has been removed from here")
            except:
                print("there's nothing to do")
        
        
    def get_values_by_reference(self, events, holidays, myevents):
        self.events = events
        self.holidays = holidays
        self.myevents = myevents
        
        
        
    def closeEvent(self, event):
        event.ignore()
        self.hide()
        
        
