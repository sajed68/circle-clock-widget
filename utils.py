#!/usr/bin/python

from PyQt5.QtCore import QTime
import math
import json
from lxml import html


### These following functions are defined for CircleClock Class



def english_to_persian(text):
    eng_to_per_num = {'0':u'۰', '1':u'۱', '2':u'۲', '3':u'۳',
               '4':u'۴', '5':u'۵', '6':u'۶', '7':u'۷',
               '8':u'۸', '9':u'۹', ':':u':', '/':u' '}
    per_text = ''.join([eng_to_per_num[i] for i in text])
    
    return per_text


def Month(index_in_english):
    month = [u"فروردین",
             u"اردیبهشت",
             u"خرداد",
             
             u"تیر",
             u"امرداد",
             u"شهریور",
             
             u"مهر",
             u"آبان",
             u"آذر",
             
             u"دی",
             u"بهمن",
             u"اسفند"]
    return month[int(index_in_english)-1]


def get_persian_day(day):
    persiandays = [u"دوشنبه",
                   u"سه‌شنبه",
                   u"چهارشنبه",
                   u"پنجشنبه",
                   u"جمعه",
                   u"شنبه",
                   u"یکشنبه"]
    return persiandays[int(day) - 1]


def __rotate_the_points__(a, x, y):
    c = y * math.cos(a) + x * math.sin(-1 * a)
    v = -y * math.sin(-a) + x * math.cos(a)
    return c, v


def make_eventquery(day, month):
    return english_to_persian(str(day)) + " " + Month(month)


def scaleinto100(a):
    return a * 100 / 255


def scaleinto255(a):
    return a * 255 / 100


def explorinhtml(page):
    tree = html.fromstring(page)
    tt = tree.xpath('//li/text()')
    if len(tt) == 0:
        return ''
    else:
        elements = [tt[k] for k in range(len(tt)) if k % 3 == 1]
        elements = [c.split('\r\n')[1] for c in elements]
        spaces = '                            ' 
        elements = [c.split(spaces)[-1] for c in elements]
        out = u''
        for c in elements:
            out = out + c + u' - '
        return out
        
