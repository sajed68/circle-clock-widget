#!/usr/bin/python
# -*- coding: utf-8 -*-

# ***************************License:***********************************
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Author: Sajed Rakhshani
# E-mail: SajedRakhshani@msn.com
# gitlab: https://gitlab.com/sajed68/circle-clock-widget
# Start: 13 Mehr 1396
# First Release 13 Mehr 1396
# Third Release 11 Mordad 1397
# version 2.0.1
# Release date: 28 Aban
# #####################################################


from PyQt5.QtWidgets import QMainWindow, QWidget, QColorDialog, QDialog, QLabel, QPushButton
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtNetwork import QNetworkAccessManager, QNetworkReply, QNetworkRequest
from PyQt5.QtCore import QDate, QSize, QEventLoop, QUrl ##TODO
from PyQt5.QtGui import QColor
from PyQt5.Qt import QFont
import json ##TODO
import jdatetime
import utils
from settings_ui import Ui_Settings


class Settings(QMainWindow):
    def __init__(self):
        super(Settings, self).__init__()
        self.ui = Ui_Settings()
        self.ui.setupUi(self)
        self.config = {}
        self.events = {}
        self.holidays = {}
        
        self.sizechanged = {}
        
        self.clockfacecolor = [0]*4
        self.clockaroundcirclescolor = [0]*4
        self.clockcolor = [0]*3
        self.datecolor = [0]*3
        self.second_color = [0]*3
        self.seconds_color = [0]*3
        self.showsecondpoint = True
        self.showiconbool = True
        
        self.clockwidth = 0
        
        self.aboutmedialog = QDialog(self)
        
        self.daynums = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]
        
        self.ui.colorclock_s_a.valueChanged.connect(self.colorclockslider_a_changed)
        self.ui.colorclock_s_r.valueChanged.connect(self.colorclockslider_r_changed)
        self.ui.colorclock_s_g.valueChanged.connect(self.colorclockslider_g_changed)
        self.ui.colorclock_s_b.valueChanged.connect(self.colorclockslider_b_changed)
        
        self.ui.coloraround_s_a.valueChanged.connect(self.coloraroundslider_a_changed)
        self.ui.coloraround_s_r.valueChanged.connect(self.coloraroundslider_r_changed)
        self.ui.coloraround_s_g.valueChanged.connect(self.coloraroundslider_g_changed)
        self.ui.coloraround_s_b.valueChanged.connect(self.coloraroundslider_b_changed)
        
        self.ui.clockfontname.currentFontChanged.connect(self.clockfontname_changed)
        self.ui.datefontname.currentFontChanged.connect(self.datefontname_changed)
        self.ui.eventfontname.currentFontChanged.connect(self.eventfontname_changed)
        
        self.ui.clockfont_size.valueChanged.connect(self.clockfontsize_changed)
        self.ui.datefont_size.valueChanged.connect(self.datefontsize_changed)
        
        self.ui.clockfontcolor_b.clicked.connect(self.pickclockcolor)
        self.ui.datefontcolor_b.clicked.connect(self.pickdatecolor)
        
        self.ui.secondcolor_b.clicked.connect(self.picksecondcolor)
        self.ui.secondscolor_b.clicked.connect(self.picksecondscolor)
        
        self.ui.secondshow.toggled.connect(self.showsecondpointoption)
        
        self.ui.clockwidth_s.valueChanged.connect(self.changeclockwidth)
        
        self.ui.aboutme_b.clicked.connect(self.aboutme)
        
        self.ui.updater_b.clicked.connect(self.eventupdater)
        
        self.ui.submit_b.clicked.connect(self.write_config)
        
        self.ui.discard_b.clicked.connect(self.read_config)
        
        self.ui.iconshowcheck.toggled.connect(self.showiconontaskbar)
        
        
        self.aboutmedialog.setWindowTitle(u"درباره")
        self.aboutmedialog.setMinimumSize(QSize(200, 250))
        self.aboutmedialog.setMaximumSize(QSize(200, 250))
        l = QLabel(self.aboutmedialog)
        aboutmetext = "<p><h2><center> .::Circleclock::. </center></h2></p> <p> <h3><center> تقویم فارسی </center></h3></p> <p><b> Author:</b> <a href='https://gitlab.com/users/sajed68/projects'><b>SajedRakhshani</b></a> </p> <p> <mailto><a href='mailto:sajedrakhshani@msn.com'> <b> E-mail </b></a></mailto></p> <p> LICENCE: <a href=https://gitlab.com/sajed68/circle-clock-widget/blob/final/LICENSE> GNU GPLv4</a> "
        l.setText(aboutmetext)
        l.move(0,0)
        pb = QPushButton(self.aboutmedialog)
        pb.setText(u"باش!")
        pb.setMinimumSize(200, 50)
        pb.setMaximumSize(200, 50)
        pb.move(0,200)
        pb.clicked.connect(self.aboutmedialog.close)
        
             
    def __update__(self):
        self.ui.colorclock_s_a.setValue(utils.scaleinto100(self.config["clockfacecolor"][3]))
        self.ui.colorclock_s_r.setValue(utils.scaleinto100(self.config["clockfacecolor"][0]))
        self.ui.colorclock_s_g.setValue(utils.scaleinto100(self.config["clockfacecolor"][1]))
        self.ui.colorclock_s_b.setValue(utils.scaleinto100(self.config["clockfacecolor"][2]))
        
        self.ui.coloraround_s_a.setValue(utils.scaleinto100(self.config["clockaroundcirclescolor"][3]))
        self.ui.coloraround_s_r.setValue(utils.scaleinto100(self.config["clockaroundcirclescolor"][0]))
        self.ui.coloraround_s_g.setValue(utils.scaleinto100(self.config["clockaroundcirclescolor"][1]))
        self.ui.coloraround_s_b.setValue(utils.scaleinto100(self.config["clockaroundcirclescolor"][2]))
        
        self.ui.clockfontname.setCurrentFont(QFont(self.config["clockfont_name"], self.config["clockfontsize"]))
        self.ui.datefontname.setCurrentFont(QFont(self.config["datefont_name"], self.config["datefontsize"]))
        self.ui.eventfontname.setCurrentFont(QFont(self.config["eventfont_name"], self.config["eventfontsize"]))
        
        self.ui.clockfont_size.setValue(self.config["clockfontsize"])
        self.ui.datefont_size.setValue(self.config["datefontsize"])
        self.ui.eventfont_size.setValue(self.config["eventfontsize"])
        
        self.clockcolor = self.config['clockcolor']
        self.datecolor = self.config['datecolor']
        
        clockcolorname = QColor(self.clockcolor[0], self.clockcolor[1], self.clockcolor[2]).name()
        datecolorname = QColor(self.datecolor[0], self.datecolor[1], self.datecolor[2]).name()
        
        self.ui.clockfontcolor_b.setStyleSheet("QPushButton {background-color: "+clockcolorname+";color:"+clockcolorname+";}")
        self.ui.datefontcolor_b.setStyleSheet("QPushButton {background-color: "+datecolorname+";color:"+datecolorname+";}")
        
        self.second_color = self.config["second_color"]
        self.seconds_color = self.config["seconds_color"]
        
        secondcolorname = QColor(self.second_color[0], self.second_color[1], self.second_color[2]).name()
        secondscolorname = QColor(self.seconds_color[0], self.seconds_color[1], self.seconds_color[2]).name()
        
        self.ui.secondcolor_b.setStyleSheet("QPushButton {background-color: "+secondcolorname+";color:"+secondcolorname+ ";}")
        self.ui.secondscolor_b.setStyleSheet("QPushButton {background-color: "+secondscolorname+";color:"+secondscolorname+ ";}")
        
        self.clockwidth = self.config["clockwidth"]
        self.ui.clockwidth_s.setValue(self.clockwidth)
        
        self.ui.updater_pb.setValue(0)
        self.ui.updater_pb.setTextVisible(False)
        self.ui.updater_pb.setVisible(False)
        
        self.showiconbool = self.config["icon"]
        self.ui.iconshowcheck.setChecked(self.showiconbool)
        
        #Disable Buttons:
        self.ui.submit_b.setEnabled(False)
        self.ui.discard_b.setEnabled(False)
        
        
    # *******
    def colorclockslider_a_changed(self):
        a = int(self.ui.colorclock_s_a.value())
        a = int(utils.scaleinto255(a))
        self.ui.colorclock_v_a.setText(utils.english_to_persian(str(a)))
        self.clockfacecolor = self.config['clockfacecolor']
        self.clockfacecolor[3] = a
        self.config['clockfacecolor'] = self.clockfacecolor
        #submit button enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def colorclockslider_r_changed(self):
        r = int(self.ui.colorclock_s_r.value())
        r = int(utils.scaleinto255(r))
        self.ui.colorclock_v_r.setText(utils.english_to_persian(str(r)))
        self.clockfacecolor = self.config['clockfacecolor']
        self.clockfacecolor[0] = r
        self.config['clockfacecolor'] = self.clockfacecolor
        #submit button enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def colorclockslider_g_changed(self):
        g = int(self.ui.colorclock_s_g.value())
        g = int(utils.scaleinto255(g))
        self.ui.colorclock_v_g.setText(utils.english_to_persian(str(g)))
        self.clockfacecolor = self.config['clockfacecolor']
        self.clockfacecolor[1] = g
        self.config['clockfacecolor'] = self.clockfacecolor
        #submit button enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def colorclockslider_b_changed(self):
        b = int(self.ui.colorclock_s_b.value())
        b = int(utils.scaleinto255(b))
        self.ui.colorclock_v_b.setText(utils.english_to_persian(str(b)))
        self.clockfacecolor = self.config['clockfacecolor']
        self.clockfacecolor[2] = b
        self.config['clockfacecolor'] = self.clockfacecolor
        #submit button enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    # ********
    def coloraroundslider_a_changed(self):
        a = int(self.ui.coloraround_s_a.value())
        a = int(utils.scaleinto255(a))
        self.ui.coloraround_v_a.setText(utils.english_to_persian(str(a)))
        self.clockaroundcirclescolor = self.config['clockaroundcirclescolor']
        self.clockaroundcirclescolor[3] = a
        self.config['clockaroundcirclescolor'] = self.clockaroundcirclescolor
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def coloraroundslider_r_changed(self):
        r = int(self.ui.coloraround_s_r.value())
        r = int(utils.scaleinto255(r))
        self.ui.coloraround_v_r.setText(utils.english_to_persian(str(r)))
        self.clockaroundcirclescolor = self.config['clockaroundcirclescolor']
        self.clockaroundcirclescolor[0] = r
        self.config['clockaroundcirclescolor'] = self.clockaroundcirclescolor
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def coloraroundslider_g_changed(self):
        g = int(self.ui.coloraround_s_g.value())
        g = int(utils.scaleinto255(g))
        self.ui.coloraround_v_g.setText(utils.english_to_persian(str(g)))
        self.clockaroundcirclescolor = self.config['clockaroundcirclescolor']
        self.clockaroundcirclescolor[1] = g
        self.config['clockaroundcirclescolor'] = self.clockaroundcirclescolor
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def coloraroundslider_b_changed(self):
        b = int(self.ui.coloraround_s_b.value())
        b = int(utils.scaleinto255(b))
        self.ui.coloraround_v_b.setText(utils.english_to_persian(str(b)))
        self.clockaroundcirclescolor = self.config['clockaroundcirclescolor']
        self.clockaroundcirclescolor[2] = b
        self.config['clockaroundcirclescolor'] = self.clockaroundcirclescolor
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
    # ********
    def clockfontname_changed(self):
        self.config["clockfont_name"] = self.ui.clockfontname.currentText()
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
    
        
    def datefontname_changed(self):
        self.config["datefont_name"] = self.ui.datefontname.currentText()
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def eventfontname_changed(self):
        self.config["eventfont_name"] = self.ui.eventfontname.currentText()
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    # *********
    def clockfontsize_changed(self):
        self.config["clockfontsize"] = self.ui.clockfont_size.value()
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def datefontsize_changed(self):
        self.config["datefontsize"] = self.ui.datefont_size.value()
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def eventfontsize_changed(self):
        self.config["datefontsize"] = self.ui.clockfont_size.value()
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    # *************
    def pickclockcolor(self):
        self.clockcolor = self.config["clockcolor"]
        pickedcolor = QColorDialog.getColor(QColor(self.clockcolor[0], self.clockcolor[1], self.clockcolor[2]))
        if pickedcolor.name() != QColor(self.clockcolor[0], self.clockcolor[1], self.clockcolor[2]).name():
            self.clockcolor[0] = pickedcolor.red()
            self.clockcolor[1] = pickedcolor.green()
            self.clockcolor[2] = pickedcolor.blue()
            clockcolorname = QColor(self.clockcolor[0], self.clockcolor[1], self.clockcolor[2]).name()
            self.ui.clockfontcolor_b.setStyleSheet("QPushButton {background-color: " + clockcolorname + ";color:" + clockcolorname+";}")
            #buttons enabled:
            self.ui.submit_b.setEnabled(True)
            self.ui.discard_b.setEnabled(True)
        
        
    def pickdatecolor(self):
        self.datecolor = self.config["datecolor"]
        pickedcolor = QColorDialog.getColor(QColor(self.datecolor[0], self.datecolor[1], self.datecolor[2]))
        if pickedcolor.name() != QColor(self.datecolor[0], self.datecolor[1], self.datecolor[2]).name():
            self.datecolor[0] = pickedcolor.red()
            self.datecolor[1] = pickedcolor.green()
            self.datecolor[2] = pickedcolor.blue()
            datecolorname = QColor(self.datecolor[0], self.datecolor[1], self.datecolor[2]).name()
            self.ui.datefontcolor_b.setStyleSheet("QPushButton {background-color: " + datecolorname + ";color:" + datecolorname+";}")
            #buttons enabled:
            self.ui.submit_b.setEnabled(True)
            self.ui.discard_b.setEnabled(True)
        
        
    # ************
    def picksecondcolor(self):
        self.second_color = self.config["second_color"]
        pickedcolor = QColorDialog.getColor(QColor(self.second_color[0], self.second_color[1], self.second_color[2]))
        if pickedcolor.name() != QColor(self.second_color[0], self.second_color[1], self.second_color[2]).name():
            self.second_color[0] = pickedcolor.red()
            self.second_color[1] = pickedcolor.green()
            self.second_color[2] = pickedcolor.blue()
            second_colorname = QColor(self.second_color[0], self.second_color[1], self.second_color[2]).name()
            self.ui.secondcolor_b.setStyleSheet("QPushButton {background-color: " + second_colorname + ";color:" + second_colorname+";}")
            #buttons enabled:
            self.ui.submit_b.setEnabled(True)
            self.ui.discard_b.setEnabled(True)
        
        
    def picksecondscolor(self):
        self.seconds_color = self.config["seconds_color"]
        pickedcolor = QColorDialog.getColor(QColor(self.seconds_color[0], self.seconds_color[1], self.seconds_color[2]))
        if pickedcolor.name() != QColor(self.seconds_color[0], self.seconds_color[1], self.seconds_color[2]).name():
            self.seconds_color[0] = pickedcolor.red()
            self.seconds_color[1] = pickedcolor.green()
            self.seconds_color[2] = pickedcolor.blue()
            seconds_colorname = QColor(self.seconds_color[0], self.seconds_color[1], self.seconds_color[2]).name()
            self.ui.secondscolor_b.setStyleSheet("QPushButton {background-color: " + seconds_colorname + ";color:" + seconds_colorname+";}")
            #buttons enabled:
            self.ui.submit_b.setEnabled(True)
            self.ui.discard_b.setEnabled(True)
        
        
        
    def showsecondpointoption(self):
        self.showsecondpoint = self.ui.secondshow.isChecked()
        self.config["showsecondpoint"] = self.showsecondpoint
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def showiconontaskbar(self):
        self.showiconbool = self.ui.iconshowcheck.isChecked()
        self.config["icon"] = self.showiconbool
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def changeclockwidth(self):
        self.clockwidth = self.ui.clockwidth_s.value()
        self.ui.clockwisth_v.setText(utils.english_to_persian(str(self.clockwidth)))
        self.config["clockwidth"] = self.clockwidth
        self.sizechanged[0] = not self.sizechanged[0]
        #buttons enabled:
        self.ui.submit_b.setEnabled(True)
        self.ui.discard_b.setEnabled(True)
        
        
    def aboutme(self):
        self.aboutmedialog.show()
        
        
    def eventupdater(self):
        self.ui.updater_pb.setVisible(True)
        self.ui.updater_b.setEnabled(False)
        date = QDate.currentDate()
        currentday = date.day()
        currentmonth = date.month()
        currentyear = date.year()
        shamsi = jdatetime.GregorianToJalali(currentyear, currentmonth, currentday)
        year = str(shamsi.jyear)
        month = str(shamsi.jmonth)
        day = str(shamsi.jday)
        isleapyear = date.isLeapYear(currentyear)
        if isleapyear:
            self.daynums[-1] = 30
        manager = QNetworkAccessManager()
        #response = QNetworkReply(self)
        event = QEventLoop()
        i = 0
        poorconnection = False
        for m in range(1, 13):
            if poorconnection:
                break
            for d in range(1, self.daynums[m-1]+1):
                request = "http://www.time.ir/fa/event/list/0/" + year + "/" + str(m) + "/" + str(d)
                response = manager.get(QNetworkRequest(QUrl(request)))
                response.finished.connect(event.quit)
                event.exec()
                if response.size() == 0:
                    print('poor internet connection')
                    poorconnection = True
                    break
                html = response.readAll().data().decode('utf-8')
                
                isholiday = html.find("eventHoliday") < 20000 and html.find("eventHoliday") != -1
                eventquery = utils.english_to_persian(str(d)) + " " + utils.Month(m)
                try:
                    self.holiday.pop(eventquery)
                    self.events.pop(eventquery)
                except:
                    pass
                out = utils.explorinhtml(html)
                if isholiday:
                    out += u"(تعطیل)"
                    self.holidays[eventquery] = eventquery
                if len(out) > 1:
                    self.events[eventquery] = out
                print(eventquery + " : " + out)
                self.ui.updater_pb.setValue(int(99*i/366))
                i += 1
                
        with open('events.json', 'w', encoding="utf8") as outfile:
            json.dump(self.events, outfile)
                
        with open('holidays.json', 'w', encoding="utf8") as outfile:
            json.dump(self.holidays, outfile)
        self.ui.updater_pb.setVisible(False)
        self.ui.updater_b.setEnabled(True)
                
        
        
    def write_config(self):
        with open('config.json', 'w') as outfile:
            json.dump(self.config, outfile)
        
    def read_config(self):
        with open('config.json', 'r') as configfile:
            config = json.load(configfile)
            for key in config.keys():
                self.config[key] = config[key]
        self.__update__()
        
    
    def get_values_by_reference(self, config, events, holidays, sizechanged):
        self.config = config
        self.events = events
        self.holidays = holidays
        self.sizechanged = sizechanged
        
        
    def closeEvent(self, event):
        event.ignore()
        self.hide()
        
